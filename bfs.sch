EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:teensy
LIBS:cherry_mx_switch
LIBS:bfs-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x04 J1
U 1 1 5A5F5033
P 6050 1250
F 0 "J1" H 6050 1450 50  0000 C CNN
F 1 "Conn_01x04" H 6050 950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 6050 1250 50  0001 C CNN
F 3 "" H 6050 1250 50  0001 C CNN
	1    6050 1250
	1    0    0    -1  
$EndComp
Text GLabel 5750 1250 0    60   Input ~ 0
VBUS
Text GLabel 5750 1350 0    60   Input ~ 0
D+
Text GLabel 5750 1450 0    60   Input ~ 0
D-
Text GLabel 7200 2050 0    60   Input ~ 0
D-
Text GLabel 7200 1950 0    60   Input ~ 0
D+
Text GLabel 7200 1850 0    60   Input ~ 0
VBUS
$Comp
L cherry_mx_switch U2
U 1 1 5A5F7865
P 5950 2950
F 0 "U2" H 5950 3200 60  0000 C CNN
F 1 "cherry_mx_switch" H 5950 2700 60  0000 C CNN
F 2 "kailh_big_series:kailh_big_series" H 5950 2950 60  0001 C CNN
F 3 "" H 5950 2950 60  0001 C CNN
	1    5950 2950
	1    0    0    -1  
$EndComp
Text GLabel 4900 2850 0    60   Input ~ 0
VCC
Text GLabel 2950 1750 2    60   Input ~ 0
VCC
$Comp
L GND #PWR01
U 1 1 5A5F7F29
P 1350 1550
F 0 "#PWR01" H 1350 1300 50  0001 C CNN
F 1 "GND" H 1350 1400 50  0000 C CNN
F 2 "" H 1350 1550 50  0001 C CNN
F 3 "" H 1350 1550 50  0001 C CNN
	1    1350 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 1150 5850 1150
Wire Wire Line
	5850 1250 5750 1250
Wire Wire Line
	5850 1350 5750 1350
Wire Wire Line
	5850 1450 5750 1450
Wire Wire Line
	7300 2050 7200 2050
Wire Wire Line
	7300 1950 7200 1950
Wire Wire Line
	7300 1850 7200 1850
Wire Wire Line
	2950 1750 2850 1750
Wire Wire Line
	4900 2850 5400 2850
$Comp
L R R1
U 1 1 5A5F80FB
P 5150 3050
F 0 "R1" V 5230 3050 50  0000 C CNN
F 1 "150R" V 5150 3050 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5080 3050 50  0001 C CNN
F 3 "" H 5150 3050 50  0001 C CNN
	1    5150 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	6500 3050 7000 3050
Text GLabel 4900 3050 0    60   Input ~ 0
LED_R
Text GLabel 4900 2950 0    60   Input ~ 0
SW
Wire Wire Line
	4900 2950 5400 2950
Text GLabel 2950 2650 2    60   Input ~ 0
SW
Text GLabel 2950 2450 2    60   Input ~ 0
LED_R
Text GLabel 7000 3050 2    60   Input ~ 0
VCC
Wire Wire Line
	5400 3050 5300 3050
Wire Wire Line
	4900 3050 5000 3050
$Comp
L R R3
U 1 1 5A61E60E
P 6750 2950
F 0 "R3" V 6830 2950 50  0000 C CNN
F 1 "100R" V 6750 2950 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6680 2950 50  0001 C CNN
F 3 "" H 6750 2950 50  0001 C CNN
	1    6750 2950
	0    1    1    0   
$EndComp
$Comp
L R R2
U 1 1 5A61E6A9
P 6750 2850
F 0 "R2" V 6830 2850 50  0000 C CNN
F 1 "100R" V 6750 2850 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6680 2850 50  0001 C CNN
F 3 "" H 6750 2850 50  0001 C CNN
	1    6750 2850
	0    1    1    0   
$EndComp
Text GLabel 7000 2850 2    60   Input ~ 0
LED_B
Text GLabel 7000 2950 2    60   Input ~ 0
LED_G
Wire Wire Line
	6900 2850 7000 2850
Wire Wire Line
	6900 2950 7000 2950
Wire Wire Line
	6500 2850 6600 2850
Wire Wire Line
	6500 2950 6600 2950
Text GLabel 2950 2550 2    60   Input ~ 0
LED_G
Text GLabel 2950 2750 2    60   Input ~ 0
LED_B
Wire Wire Line
	2850 2450 2950 2450
Wire Wire Line
	2950 2550 2850 2550
Wire Wire Line
	2850 2750 2950 2750
Wire Wire Line
	2850 2650 2950 2650
$Comp
L GND #PWR02
U 1 1 5A674E44
P 5350 1200
F 0 "#PWR02" H 5350 950 50  0001 C CNN
F 1 "GND" H 5350 1050 50  0000 C CNN
F 2 "" H 5350 1200 50  0001 C CNN
F 3 "" H 5350 1200 50  0001 C CNN
	1    5350 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 1150 5350 1200
$Comp
L Teensy2.0 U1
U 1 1 5A675213
P 2150 2200
F 0 "U1" H 2150 3150 60  0000 C CNN
F 1 "Teensy2.0" H 2150 1250 60  0000 C CNN
F 2 "teensy:Teensy2.0" H 2250 1150 60  0000 C CNN
F 3 "" H 2250 1150 60  0000 C CNN
	1    2150 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 1450 1350 1450
NoConn ~ 1450 2950
Wire Wire Line
	1350 1450 1350 1550
$EndSCHEMATC
